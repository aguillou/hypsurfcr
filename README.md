# HypSurfCR: Tools for experimenting and visualizing limit sets of PU(2,1)-representations of hyperbolic surface groups


## Introduction

The goal of this project is to build a comprehensive set of tools for experimentation and visualization of limit sets of hyperbolic surface groups. 

One can try and compute directly a very good approximation of the limit set $\Lambda$ without leveraging the structure of hyperbolic surface group. See the related project https://gitlab.inria.fr/ouragan/cr-limit-sets . Our approach here is different for the following reasons:

- We are focused on families of representations of the same group. The preparations made possible by the hyperbolic surface structure - which can be lengthy computations - allow to dramatically accelerate the computation of each limit sets in the family afterward. For our project, it is worthwhile to spend time to prepare computations with (real, plane) hyperbolic geometry, and then leverage this preparations for repeated computations of limit sets.

- This project give more information than the raw limit set. In fact, what  we really compute is an approximation of the Cannon-Thurston map $\partial_\infty \Gamma \to \Lambda$. This is by itself interesting. Together with the visualization tools, it gives an illustration of Floyd theorem and exploration around Mj-Series theorem for continuous deformation of Cannon-Thurston maps.

- An important estimation for our purposes is the supremum of Cartan invariant over the limit set. However, this estimation can prove numerically instable and very lenghty if not done carefuly. Once again, the additional information from the hyperbolic surface structure helps a lot to efficiently compute a convincing estimation of this supremum.

## Input

The input, apart from some parameters to be later described, should have the following structure:

- For the hyperbolic surface group:

    - A group $\Gamma$, i.e a finite presentation $\langle a,b,... | \mathrm{Relations}\rangle$. Each relation is a word in $a, A, b, B,$ ; following classic conventions (e.g. Snappy) the capital letter denotes the inverse of the relative element. The basic examples are triangle groups, e.g.: $\langle a,b,c | a^3 , b^3 , abc\rangle$.

    - A hyperbolic monodromy, i.e. a representation $h: \Gamma \to SU(1,1)$ whose image is a lattice. For practical purposes, it is a collection of matrices $h(a), h(b),...$ in $SU(1,1)$ such that for each word $w$ in $\mathrm{Relations}$, the corresponding matrix $h(w)$ is $\pm I_2$.

    - [Only for estimation of the sup of Cartan invariant] An effective description of a fundamental domain for $h(\Gamma)$, or at least a subset of $D = \{ |z|<1\} \subset \mathbf C$ containing a fundamental domain. It can be described e.g. as a ball, or as the intersection of a side of a finite set of geodesics... 
    
- For the PU(2,1) representations:

    - A representation $\rho : \Gamma \to PU(2,1)$, i.e. a list of matrices $\rho(A),\ldots$ which verify the $\mathrm{Relations}$. It can be a family with parameters.
    
    - It is intended that the user  warrants that the representation is indeed into $PU(2,1)$, i.e. each matrix $M$ verify: $M^t \begin{pmatrix} &&1\\ &2&\\ 1&&\end{pmatrix}\overline{M} = \begin{pmatrix} &&1\\ &2&\\ 1&&\end{pmatrix}$. Moreover, no matrix should become elliptic (there should be an attractive fixed point in the sphere). The project can work without the last assumption, but with no certified behaviour.
    
## Output

The code in this projects mainly computes an approximation of the limit set $\Lambda$ in the sphere $\left\{\begin{pmatrix} z_1\\ z_2\\ 1 \end{pmatrix}, |z_1|^2 + 2|z_2|^2 = 1 \right\}$ of the representation $\rho$. It allows for related more precise information:

- A finite (but with many points!) set $L$ in the sphere, such that each point is a certified approximation of a point in $\Lambda$.

- Each point is $L$ comes with a unique angle parameter, which gives an approximation of the Cannon-Thurston map $\partial_\infty \Gamma \simeq \mathbf S^1 \to \Lambda$. For practical purpose, $L$ is a csv file, each line being a couple `angle, point` where `angle` describe a point in $\mathbf S^1$ and `point`is approximatively its image in $\Lambda$.

- A set of tools for visualization: snapshots of stereographic projections, or Siegel projections ; construction of a vtk file for 3d visualization inside Paraview or through a web interface.

- A computation of an approximation of the sup of the Cartan invariant. More precisely, at this point, we compute a certified approximation of numerous Cartan invariants, thus giving a certified approximation of *a lower bound* of the supremum.

## Methods

The project splits naturally in three main parts:

- **Preprocessing**: all work here is done in real plane hyperbolic geometry. We compute an approximation of the homeomorphism $\partial_\infty \Gamma \simeq \mathbf S^1$. It computes a list (or csv file) of entries 
```a = angle, w = word, c = conj``` 
such that $h(c) h(w) h(c)^{-1}$ has an attractive point at angle `a`.
Once this is done, one computes a finite list of triples $(p,q,r)$ of distinct points in $\partial_\infty \Gamma \simeq\subset \mathbf S^1$ such that the projection of $r$ on the geodesic $pq$ lies inside the given fundamental domain. This list will be important to estimate the supremum of the Cartan invariant.

- **Computation of Cannon-Thurston map**: For each `a,w,c`in the previous list, one computes the attractive fixed point of $\rho(c) \rho(w) \rho(c)^{-1}$ in the sphere $\mathbf S^3$. From that, and the list of triples $(p,q,r)$ it is easy to estimate (a lower bound of) the supremum of the Cartan invariant.

- **Visualization**: A set in *some* sphere $\mathbf S^3$ has been computed. Its visualization necessitates an additional step: a projection (either stereographic or the more specific Siegel) into $\mathbf R^3$. Various tools are provided in order to easily visualize these projections: 2D snaphsots for quick inspection, 3D visualization for in-depth contemplation. 

## Prerequisites

The code works in a conda environment described in the `spec-file.txt` file

