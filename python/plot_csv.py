#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

script_gnuplot = '/home/antonin/DropSU/HyperbolicSurfaceCR/gnuplot/script-gnupic3planes.plg'

def plot_csv(path_csv_file, outname = '', gnuplot_script = script_gnuplot):
    if outname == '':
        outname = path_csv_file.replace('csv','png')
    string_gnuplot = "gnuplot -e \"filename='%s' ; outname='%s'\" %s"%(
                        path_csv_file, 
                        outname, 
                        gnuplot_script)
    print(string_gnuplot)
    os.system(string_gnuplot)
    
