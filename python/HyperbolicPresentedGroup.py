#!/usr/bin/env python
# -*- coding: utf-8 -*-

from time import time
from itertools import product
from functools import reduce
from numba import jit, vectorize, int64, complex128, float64
import pandas as pd
import numpy as np
import os
from mpmath import mp, mpc, matrix, sqrt, pi, exp, arg, norm, floor, fabs
mp.dps = 30
mp.pretty = True
I = mpc(0,1)

from GenericPresentedGroup import GenericPresentedGroup

class HyperbolicPresentedGroup(GenericPresentedGroup):
    """
    Defines a finitely presented group, with the method of PresentedGroup
    and a representation into SU(1,1) (hyperoblic geometry).
    
    Rep should be a dictionary {a:M} where a is a generator and M a matrix.
    """
    def __init__(self, Gens, Rels, Rep, admissible_error = 1e-25, 
                        name = 'my_group'):
        super().__init__(Gens, Rels, Rep, name = name, 
                    admissible_error = admissible_error, Flag_PU = False)
        self.store = self.set_store()
        self.boundary = {}
        if not ((self.dim == 2) & (self.is_PU)):#We check that we have indeed
                                            #a PU(1,1) representation.
            raise Exception('It is not a representation in PU(1,1)')
    
    def attractive_fixed_point(self, 
                                w, 
                                max_save = 'inf', 
                                adm_error = (10**5)):
        g = self.word_representation(w,max_save = max_save)
        p = matrix_attractive_fixed_point(g, 
                                        adm_error*self.admissible_error
                                        )
        return p
    
    def parametrized_boundary(self,n,resolution = 10^6):
        """
        Computation of an approximation of
        the circle at infinity as a list of 
        attractive points of element of length
        <2n of the group.
        """
        if not(n in (self.boundary).keys()):
            self.boundary[n] = {}

        if resolution in (self.boundary[n]).keys():
            computed = self.boundary[n][resolution]
        else:
            computed = compute_parametrization(self,n,resolution)
            self.boundary[n][resolution] = computed
        return computed

        
    def set_store(self, store = ''):
        if store == '':
            store = '../data/' + self.name + '/parametrizations.hdf5'
            if not os.path.exists('../data/' + self.name + '/'):
                os.mkdir('../data/' + self.name + '/')
        return store
        
    def export(self, store = ''):
        store_path = self.set_store(store)
        if store_path != self.store:
            self.store = store_path
        with pd.HDFStore(self.store, comp_level = 9, comp_lib = "blosc:lz4") as HDFstore:
            for k in self.boundary.keys():
                for res in (self.boundary[k]).keys():
                    HDFstore[f"length_{k}/resolution_{res}"]=self.boundary[k][res]
        print("Parametrizations exported to the store "+store)
    


def inv(A):
    """
    Ad hoc inversion for matrices in SL(2,C)
    
    Do not increase the errors on the coefficients
    """
    [[a,b],[c,d]] = A.tolist()
    return matrix([[d,-b],[-c,a]])

def trace(A):
    """
    Trace for matrices in M(2,C)
    
    Do not increase the errors on the coefficients
    """
    [[a,b],[c,d]] = A.tolist()
    return a+d

def apply_homography(A,z):
    [[a,b],[c,d]] = A.tolist()
    if z == 'inf':
        if c==0:
            return 'inf'
        else:
            return a/c
    else:
        den = c*z +d
        if den == 0:
            return 'inf'
        else:
            return (a*z+b)/(c*z+d)


def matrix_attractive_fixed_point(A, admissible_error):
    """
    A is a matrix of SL(2,C). We find a attractive
    fixed point of the associated homography when it is hyperbolic or
    upper triangular unipotent.
    Returns 'NAN' an exception if A seems to be elliptic or the fixed point
    is not on the circle.
    """
    [[a,b],[c,d]] = A.tolist()
    Delta = (a+d)**2 - 4
    if Delta == 0:
        if c == 0:
            if d==0:
                return 'NAN'
            else:
                return 'inf'
        else:
            p = (a-d)/(2*c)
    else:
        delta = sqrt(Delta)
        l1 = a+d+delta
        l2 = a+d-delta
        if abs(abs(l1)-abs(l2)) < admissible_error:
            return 'NAN'
        elif abs(l1)>abs(l2):
            p = (a-d + delta)/(2*c)
        else:
            p = (a-d - delta)/(2*c)  
    if abs(abs(p) - 1)>admissible_error:
        return 'NAN'
    else:
        return p

def compute_parametrization(self,n,resolution):
    """
    Computes a pandas dataframe whose columns are
    'Angle', 'Word', 'Conjugator'
    
    The angle is an integer k with |k| <=resolution/2.
    It represents the interval [pi+2pi k/resolution,pi+2pi (k+1)/resolution]
    
    The Word w and Conjugator c mean that for the hyperbolic action,
    the matrix associated to cwc^-1 has an attractive fixed point whose argument
    lies in the previous interval. Note that this fixed point is the image c(p) 
    where p is the attractive fixed point of w.
    """

    print(f"Computing parametrization of boundary at infinity with length at most {4*n}")

    #First we retrieve the parabolic and hyperbolic elements in the hyperbolic group
    print(f"    Finding fixed points for length at most {2*n}")
    interval = 2*pi/resolution
    fixed_points = {}
    dt_angles = {}
    K = dt_angles.keys()
    
    
    #We try and retrieve the attractive fixed points
    # for words of len <= 2N
    word_length_2n = filter(lambda w:w!='',
                            self.ElementsMaxLength(2*n))
    attractive_fixed_points = filter(
                lambda x : type(x[1])!=str,
                map(
                        lambda w : (w, self.attractive_fixed_point(w,max_save = n)),
                        word_length_2n
                    )
                                    )
    
    for w, p in attractive_fixed_points:
    #We only choose the smallest w for each interval found.
        angle = int(floor(arg(p)/interval))
        if not (angle in K): 
            fixed_points[w] = p
            dt_angles[angle] = (w,'')
            
    list_attractives = [key[0] for key in dt_angles.values()]
    Old_points = np.array([
        [fixed_points[key],1] for key in list_attractives],
        dtype = np.cdouble).transpose()
    
    print("    Conjugation")
    #We enrich this list of angles by conjugating
    conjugators = filter(lambda w:w!='',
                            self.ElementsMaxLength(n))
    np_K = np.array(list(K))
    for key_h in conjugators:
        #t0 = time()
        h = np.array(self.word_representation(key_h,max_save = n),
                            dtype = np.cdouble).reshape(2,2)
        h_old_points = np.dot(h, Old_points)
        intervals = interval_find(
                        h_old_points[0,:]/h_old_points[1,:],
                        resolution,
                        np.float64(pi)
                                        )
        #t1 = time()
        isNew = np.isin(intervals, np_K, invert=True)
        if np.any(isNew):
            list_new_attractives = [list_attractives[i] 
                                            for i, b in enumerate(isNew)
                                                if b]
            unique_new_i = pd.DataFrame({'i':intervals[isNew],
                                    'g':list_new_attractives}).groupby('i').min()
            np_K = np.append(np_K, unique_new_i.index)
            filtered_new_i = filter(lambda i: not(i in K),
                                unique_new_i.index)
            dt_angles.update({new_i:(unique_new_i.g.loc[new_i], key_h)
                                for new_i in filtered_new_i})
        #t2 = time()
        #print(f"        {key_h}, {len(K)}, {t1-t0},{t2-t1}")
    return pd.DataFrame([[i,w,c] for i,(w,c) in dt_angles.items()],
        columns = ['Angle','Word',"Conjugator"]).set_index('Angle').sort_index()


@vectorize([int64(complex128,int64, float64)])
def interval_find(p,resolution,PI):
    interval = 2*PI/resolution
    return int(np.floor(np.angle(p)/interval))
