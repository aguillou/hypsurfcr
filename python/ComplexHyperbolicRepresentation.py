#!/usr/bin/env python
# -*- coding: utf-8 -*-


from mpmath import matrix
import numpy as np
from scipy.linalg import eig
from GenericPresentedGroup import GenericPresentedGroup
import pandas as pd
import os
from time import time

from numba import jit

C_DTYPE = np.cdouble
C_DTYPE_2 = np.clongdouble
R_DTYPE = np.double

class PU21ParametrizedRepresentation(GenericPresentedGroup):
    """docstring for DeformTriangleSolution."""

    def __init__(self, Gens, Rels,
                PU21_Repr,
                admissible_error = 1e-25,
                name = 'my_group',
                param_store = '',
                param_key = '',
                param_name = 'a_parametrized representation'):
        super().__init__(Gens, Rels, PU21_Repr, name = name,
                            admissible_error = admissible_error, 
                            Flag_PU = False)
        if not ((self.is_PU) & (self.dim==3)):
            raise Exception('Not a PU(2,1) representation')
            
        self.parameter_name = param_name
        if param_store == '':
            param_store = '../data/' + self.name + '/parametrizations.hdf5'
        try:
            with pd.HDFStore(param_store) as store:
                self.parametrizations = list(store.keys())
                self.param_store = param_store
        # it is a hdf5 store containing
        # computed parametrizations, each one is a csv file
        # with columns Angle, Word, Conjugator
        except:
            raise Exception('No precomputed parametrizations found!')
        #print(f"The following parametrizations have been found \
        #                {self.parametrizations}")
        
        self.set_parametrization(param_key)
        self.load_parametrization()
        
    def set_parametrization(self, param_key):
        if param_key == '':
            try:
                self.param_key = min(self.parametrizations)
            except:
                raise Exception('No precomputed parametrizations found!')
        else: 
            self.param_key = param_key
        
    def load_parametrization(self):
        try:
            PD = pd.read_hdf(self.param_store, self.param_key,  
                            sep=";")
        except:
            raise Exception('Not a valid parametrization')
        self.PD_parametrization = PD
        self.Attractive_words = PD.Word.unique()
        self.Thetas = PD.index
        print(f"""Parametrization loaded: {self.param_key}. 
        {len(self.Thetas)} points, from {len(self.Attractive_words)} attractive fixed points.""")
       
    def change_representation(self, new_PU21_Repr,
                param_name = 'a_parametrized representation'):
        """
        Tool that change the representation to PU21 without reloading
        all the parametrization
        """
        New_Representation = GenericPresentedGroup(self.Generators, 
                                                    self.Relations, 
                                                    new_PU21_Repr, 
                                                    name = self.name,
                                                    admissible_error = self.admissible_error, 
                                                    Flag_PU = False)
        if not ((New_Representation.is_PU) & (New_Representation.dim==3)):
            raise Exception('Not a PU(2,1) representation')
        self.Representation = New_Representation.Representation
        self.__CachedMatrices__ = New_Representation.__CachedMatrices__
        self.parameter_name = param_name
    
    def export_csv(self, path_csv = '', directory_csv = '', dim = '3d', compress = False, 
                    projection = "SIEGEL", prec = 6):
        """
        Outil d'export des points calculés en csv, éventuellement compressé.
    
        Si dim = '3d', on exporte les points après projection stéréographique ou Siegel.
        Si dim = '4d', on exporte les coordonnées dans CP2

        Par défaut, le nom du fichier sera construit comme:
        directory_csv + self.parameter_name + "_" + dim .csv (ou .bz2 si on exporte
                                                                      compressé).
        On peut choisir un nom arbitraire avec le paramètre path_csv.

        compress est un booléen qui décide si on compresse les fichiers en bz2
        """
        
        if directory_csv == '':
            directory_csv = f'../data/{self.name}/csv/'
        if not os.path.exists(directory_csv):
            os.mkdir(directory_csv)
        if path_csv == '':
            path_csv = directory_csv + self.parameter_name + '_' + dim + ".csv"
        float_format = f"%.{prec}f"
        if dim == '3d':
            if projection == "STEREOGRAPHIC":
                try:
                    DF = np.around(self.stereographic_projection(),prec)
                except:
                    raise Exception('Cannot compute the Siegel limit set')

                print('Exporting')
                # We drop consecutive duplicates
                DF = DF.loc[(DF.shift()!=DF).any(axis=1)]
                if compress:
                    DF.to_csv(path_csv[:-3] + 'bz2',
                     sep = ',',
                     float_format = float_format,
                     index_label = "theta",
                     compression = 'bz2'
                            )
                else:
                    DF.to_csv(path_csv,
                     sep = ',',
                     float_format = float_format,
                     index_label = "theta"
                            )
                    
            elif projection == "SIEGEL":
                try:
                    DF = np.around(self.siegel_projection(),prec)
                except:
                    raise Exception('Cannot compute the Siegel limit set')
                
                print('Exporting')    
                # We drop consecutive duplicates
                DF = DF.loc[(DF.shift()!=DF).any(axis=1)]
                if compress:
                    DF.to_csv(path_csv[:-3] + 'bz2',
                     sep = ',',
                     float_format = float_format,
                     index_label = "theta",
                     compression = 'bz2'
                            )
                else:
                    DF.to_csv(path_csv,
                     sep = ',',
                     float_format = float_format,
                     index_label = "theta"
                        )
            
            else:
                print('Not well defined projection')
                
        elif dim == '4d':
            try:
                PPP = self.Parametrized_projective_points
            except:
                self.compute_limit_set()
                PPP = self.Parametrized_projective_points

            DF = pd.DataFrame.from_dict(
                {key : p.reshape(3,)[:2] for key, p in PPP.items()}, 
                orient ='index',
                columns = ['z1', 'z2'])
            a = np.real(DF['z1'])
            b = np.imag(DF['z1'])
            c = np.real(DF['z2'])
            d = np.imag(DF['z2'])    
            New_DF = pd.DataFrame(
                    np.array([a,b,c,d]).transpose(),
                    columns = ['a','b','c','d'], 
                    index = DF.index).sort_index()
            # We drop consecutive duplicates
            New_DF = New_DF.loc[(New_DF.shift()!=New_DF).any(axis=1)]
            if compress:
                New_DF.to_csv(path_csv[:-3] + "bz2",
                    sep = ',',
                    float_format = "%.10f",
                    index_label = "theta",
                    compression = 'bz2'
                    )
            else:
                New_DF.to_csv(path_csv,
                    sep = ',',
                    float_format = "%.6f",
                    index_label = "theta")
        else:
            print("dim parameter is either '3d' or '4d'")
            
    def compute_limit_set(self, parametrization_key = ''):
        """
        Computes a dictionnary {theta: point} where, for all theta in
        the parameters, point is the fixed point of hgh^{-1}, with 
        angles2words[theta] = (g,h)
        
        This dictionnary is stored in self.Parametrized_projective_points
        """
        if not ((parametrization_key == '') | 
                        (parametrization_key == self.param_key)):
            self.set_parametrization(parametrization_key)
            self.load_parametrization()
        
        tobeComputed = set(self.PD_parametrization.Word.unique()).union(
                                set(self.PD_parametrization.Conjugator.unique())
                                                                    )
        t0=time()                                                    
        self.npMatrices = {key:np.array(self.word_representation(key), 
                                        dtype=C_DTYPE_2).reshape(3,3)
                                        for key in tobeComputed}
        
        
        # We try and compute attractive fixed points, and filter those
        # for which no result is found.                                
        map_attractive_points = filter(lambda l: l[1] != 'inf',
                                    map(
                lambda key: [key] + fixed_point(self.npMatrices[key], key = key),
                self.Attractive_words
                                    )
                                        )
        ### Pourrait être parallélisé                                
                                        
                                        
        t1 =time()
        self.Parametrized_projective_points = {}
        self.Erreurs = {}
        PD_grouped = (self.PD_parametrization).groupby('Word',sort = False)
        
        #Pourrait-être parallélisée
        for key_g, err_g, p_g in map_attractive_points:
            PD_g = PD_grouped.get_group(key_g)
            n_h = len(PD_g)
            Mat_h = np.concatenate([self.npMatrices[key_h]
                                        for key_h in list(PD_g.Conjugator)])
            # On doit calculer M_h p_g pour M_h la matrice associée aux h.
            # On le fait en construisant la matrice Mat_h, qui empile les M_h

            new_points = np.dot(Mat_h,p_g).reshape(n_h,3).transpose()
            #On les multiplie par self.M pour les renvoyer dans la sphère

            err_multiplier = 1/(np.abs(new_points[2]).min())#Erreur quand on renormalise
            new_points_normalized = new_points/new_points[2]
            
            self.Erreurs[key_g] = err_g*err_multiplier
            
            self.Parametrized_projective_points.update(
                {PD_g.index[i]: new_points_normalized[:,i] 
                                            for i in range(n_h)}
                                                    )
        t2=time()        
        self.n_points = len(self.Parametrized_projective_points.keys())
        print(f"""Limit set computed, with {self.n_points} points
        {t2-t0}: {t1-t0} puis {t2-t1}""")
 
     
    # Defining the projections
          
    def siegel_projection(self):
        try:
            PPP = self.Parametrized_projective_points
        except:
            print('We need to compute the limit set')
            self.compute_limit_set()
            PPP = self.Parametrized_projective_points
        siegel = np.array([
        [ -1/np.sqrt(R_DTYPE(2)) , 0., 1/np.sqrt(R_DTYPE(2)) ],
        [ 0.                     , 1., 0.],
        [ 1/np.sqrt(R_DTYPE(2))  , 0., 1/np.sqrt(R_DTYPE(2)) ]
        ],dtype=np.dtype(C_DTYPE))
        
        keys = np.array(list(PPP.keys()))
        points = np.array([PPP[key].reshape(3,) for key in keys])
        
        PPP_siegel = np.dot(points,siegel)
        PPP_siegel = 1/((PPP_siegel[:,2])[:,None])*PPP_siegel
        DF = pd.DataFrame(PPP_siegel[:,:2],
            index = keys,
            columns = ['z1', 'z2'])
        DF['x'] = np.real(DF['z2'])
        DF['y'] = np.imag(DF['z2'])
        DF['t'] = np.imag(DF['z1'])
            
        DF = (DF[['x','y','t']]).sort_index()
        return DF
        
    def stereographic_projection(self):
        """
        projection stéréographique
        projection depuis (1,0,0,0) :
        a^2 + b^2 + c^2 + d^2 = 1
        |->
        b / (1-a) , c / (1-a) , d / (1-a)
        
        rend un pandas.DataFrame taille self.n_points * 3.
        """
        try:
            PPP = self.Parametrized_projective_points
        except:
            self.compute_parametrisation()
            PPP = self.Parametrized_projective_points
        DF = pd.DataFrame.from_dict(
            {key : p.reshape(3,)[:2] for key, p in PPP.items()}, 
            orient ='index',
            columns = ['z1', 'z2'])
        a = DF['z1'].map(lambda z: z.real)
        b = DF['z1'].map(lambda z: z.imag)
        c = DF['z2'].map(lambda z: z.real)
        d = DF['z2'].map(lambda z: z.imag)    
        New_DF = pd.DataFrame(
                np.array([(b/(1-a)),(c/(1-a)),(d/(1-a))]).transpose(),
                columns = ['x','y','z'], 
                index = DF.index).sort_index()
        # We drop consecutive duplicates
        New_DF = New_DF.loc[(New_DF.shift()!=New_DF).any(axis=1)]
        return New_DF.sort_index()

                                                       

                    

                                                




    


def goldman_trace(z):
    z2 = (z*z.conjugate()).real
    return (z2 + 18) * z2 - 8*((z*z*z).real) - 27


    


def norm1(x):
    return np.linalg.norm(x,ord = 1)


def fixed_point(matrix, key = 'a matrix'):
    """ 
    Cette fonction renvoie le point fixe [x:y:1] attractif dans CP^3
    de matrix, sous l'hypothèse qu'elle est loxodromique.
    
    Pour ça on diagonalise la matrice.

    La certification consiste à vérifier que Mp - p a toutes ses
    coordonnées plus petite que EPSILON.
    """
    try:
        MM = np.array(matrix,C_DTYPE)
        eigen_M = eig(MM)
        k = np.argmax([(l*l.conjugate()).real for l in eigen_M[0]])
        vector = eigen_M[1][:,k]
    
        p = vector/vector[2]
        vector2 = np.dot(matrix,p)
        pp = vector2/vector2[2]
    
        err = norm1(p-pp).max()
    except:
        print('No fixed point for:' + key)
        err = 'inf'
        pp = 'NaV'#Not a vector
    return [err,pp]



