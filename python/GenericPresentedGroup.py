#!/usr/bin/env python
# -*- coding: utf-8 -*-

from time import time
from itertools import product
from functools import reduce
from numba import jit, vectorize, int32, complex128, float64
import pandas as pd
import numpy as np
from mpmath import mp, mpc, matrix, diag, sqrt, pi, exp, arg, norm, floor, eye
mp.dps = 30
mp.pretty = True
I = mpc(0,1)

from AbstractPresentedGroup import PresentedGroup

class GenericPresentedGroup(PresentedGroup):
    """
    Defines a finitely presented group G, with the method of PresentedGroup
    and a representation into a projective matrix group.
    
    Rep should be a dictionary {a:M} where a is a generator and M a matrix.
    
    Gives additional method if the representation has dimension:
        - 2: tries to compute a parametrization of d_{infty}G as a circle
            in this case, the representation Rep should have values into
            PU(1,1)
        - 3: If the representation Rep has values into PU(2,1), this gives
            additional tools to work with limit sets.
    """
    def __init__(self, Gens, Rels, Rep, name = 'my_group', 
                        admissible_error = 1e-25,
                        Flag_PU = True):
        super().__init__(Gens, Rels, name)
        self.admissible_error = admissible_error
        self.Representation, self.dim, self.is_PU = self.__preprocess__(Rep)
        self.__CachedMatrices__ = {k:v for k,v in (self.Representation).items()}
        if self.coherent_relations() > admissible_error:
            raise Exception('The relations are not fulfiiled')
        if Flag_PU & self.is_PU:
            if self.dim == 2:
                print('''Your representation takes value into PU(1,1)
                Use self.PU11representation() to gain access to
                additional methods
                ''')
            if self.dim == 3:
                print('''Your representation takes value into PU(2,1).
                If you have already parametrized the boundary,
                use self.PU21representation() to gain access to
                additional methods
                ''')
        
        
    def __preprocess__(self,Rep):
        dims = set(M.rows for M in Rep.values()).union(M.cols for M in Rep.values())
        dim = dims.pop()
        if dims != set([]):
            raise Exception('Not well defined representation')
        Repr = Rep.copy()
        Repr[''] = eye(dim)
        for a in self.Generators:
            if not a in Repr.keys():
                raise Exception('Not well defined Representation') 
            Repr[a] = matrix(Repr[a])
            A= a.swapcase()
            if A in Repr.keys():
                Repr[A] = matrix(Repr[A])
            else:
                Repr[A] = (matrix(Repr[a]))**-1
                
        is_PU = check_PU(Rep, dim, self.admissible_error)
        return Repr, dim, is_PU
    
    def word_representation(self, w, max_save='inf'):
        return word_computation(self,w,max_save=max_save)
    
    def coherent_relations(self):
        """
        Computes the error to identity of the relations.
        """
        I = self.word_representation('')
        err = max([
                min([norm((M - M[0,0]*I),'inf')])
                 for M in map(lambda w: self.word_representation(w),
                             self.Relations)
                    ])
        return err

        
    def PU11representation(self, name = ''):
        if name == '':
            name = self.name
        from HyperbolicPresentedGroup import HyperbolicPresentedGroup

        return HyperbolicPresentedGroup(self.Generators, 
                                        self.Relations,
                                        self.Representation, 
                                        admissible_error = self.admissible_error,
                                        name = name)
                                        
    def PU21representation(self, name = '',
                            param_store = 'my_store.hdf5',
                            param_name = 'a_parametrized representation'):
        if name == '':
            name = self.name
        from ComplexHyperbolicRepresentation import PU21ParametrizedRepresentation

        return PU21ParametrizedRepresentation(self.Generators, 
                                        self.Relations,
                                        self.Representation, 
                                        admissible_error = self.admissible_error,
                                        name = self.name,
                                        param_store = param_store,
                                        param_name = param_name)
                        
        
    


def word_computation(self,w,max_save):
    """
        Recursive computation of the representation of 
        a word.
        
        The dictionnary self.__CachedMatrices__ keeps track
        of already computed matrices to speed up computations
    """
    if w in self.__CachedMatrices__.keys():
        return self.__CachedMatrices__[w]
    else:
        l1 = int(len(w)/2)
        A = word_computation(self,w[:l1],max_save)
        B = word_computation(self,w[l1:],max_save)
        M = A * B
        if max_save == 'inf':
            self.__CachedMatrices__[w]=M
        elif len(w)<=max_save:
            self.__CachedMatrices__[w]=M
        return M

def check_PU(Rep,dim,admissible_error):
    """ 
    Checks if the representation is in a projective unitary group. 
    
    Measures, in infty-norm, for any image M of a generator, if 
    transpose_conjugate(M)JM-J is almost 0, up to an admissible_error.
    """
    
    J = diag([-1 for i in range(dim-1)] + [1])

    check = max([norm(M.transpose_conj()*J*M -J,'inf') 
                                    for M in Rep.values()])
    return check < admissible_error
