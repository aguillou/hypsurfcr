#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import cycle, product, chain
from math import ceil


class PresentedGroup:
    """
    An instance of this class is a finitely presented group, with
    a symmetrized generators set constructed from the list
    of characters Gens, an enriched set of relations constructed from
    the set of relations Rels.
    """
    def __init__(self, Gens, Rels, name = 'my_group'):
        self.name = name
        self.Generators = Gens
        self.FullGenerators = Gens + [gen.upper() for gen in Gens]
        self.Relations = set(Rels)
        self.FullRelations = enriched_relations(self)
        self.Shortenings = shortenings(self.FullRelations)
        
        self._ElementsByLength = {0:[''], 1:self.FullGenerators}
    
    
    def __repr__(self):
        return f"""Presented Group with generators {self.Generators} and
        relations {self.Relations}
        """
        

        
    ### Some class methods
        
    def simplify(self,w):
        """
        w is a word in the FullGenerators alphabet.
        
        We use self.Shortenings to find the shortest known
        word in the PresentedGroup representing the same
        element as w
        """
        return word_simplify(w,self.Shortenings)
        
                                
    def ElementsOfLength(self,n):
        """
        Recursively computes all word of size n in self and stores
        computed elements in self._ElementsByLength dictionnary.

        Returns the list of elements of known size n.
        """
        if n in self._ElementsByLength.keys():
            return self._ElementsByLength[n]
        else:
            if n%2:
                n1 = int((n-1)/2)
                n2 = n1+1 #n1+n2=n
                computed1 = self.ElementsOfLength(n1)
                computed2 = self.ElementsOfLength(n2)
            else:
                n1 = int(n/2)
                computed1 = self.ElementsOfLength(n1)
                computed2 = computed1
                
            self._ElementsByLength[n] = list(
                    filter(
                        lambda w: not simplifiable(w, self.Shortenings)[0],
                        (''.join(w) for w in product(computed1, computed2))
                            )               )
            return self._ElementsByLength[n]
    
    
    def ElementsMaxLength(self,n):
        """
        Returns an iterator over all elements of length <=n in self
        """
        return chain.from_iterable(
                map(
                    lambda i:self.ElementsOfLength(i),
                    range(n+1)
                    )

                            )
  
    def Representation(self, Rep, admissible_error = 1e-25):
        from GenericPresentedGroup import GenericPresentedGroup

        GPG = GenericPresentedGroup(self.Generators, self.Relations, Rep,
                                    admissible_error)
        return GPG

  
##### End of class methods.  
  
  
def enriched_relations(self):
    """
    Rels is a list of words giving a sufficient set of relations for 
    the presentation of the group.

    This functions add lots of redundant relations that can be 
    infered from this data: words of the form aA for any generator a,
    all cyclic permutation of relations, all inverses of relations.
    """
    Rels = self.Relations

    # We take the inverse of each relation
    # g=e iff g.inverse() = e 

    RELATIONS = Rels.union(relation.swapcase()[::-1] for 
                                        relation in Rels)

    # Cyclic permutations
    # g_1 g_2 = e iff g_2 = G_1 iff g_2 g_1 = e

    RELATIONS.update(
                    chain.from_iterable(
                            map(
                                cyclic_permutations,
                                RELATIONS.copy()
                                )
                                        )
                    )

    # word of the form aA
    # Note: we do not have to apply the two previous steps, as 
    # we take the FullGenerators
    RELATIONS.update(a+a.swapcase() for a in self.FullGenerators)

    return RELATIONS

        
def shortenings(Relations):
    """
    From a set of Relations (words in the generators), computes a dict
    of Shortenings, i.e. strings that can be shortened using the relations.
    
    If a relations is st (with length(s)>length(t)), one can replace
    s by t.inverse(). 
    If the relation st has even length an len(s)=len(t), we choose to replace 
    the longest (lexigographic order) between s and t.inverse() by the shortest, for normalization.
    (see relation_shortening below)
    
    The entries of the dictionnary are sorted by ascending length of s,
    for optimizing the simplification process.
    """
    shorts = {k:v for k,v in chain.from_iterable(map(relation_shortenings, Relations))}
    return {k: shorts[k] for k in sorted(shorts,key = len)}

def relation_shortenings(relation):
    """
    From a relation, defines a dictionnary of related shortenings.
    
    If the relation is st (with length(s)>length(t)), one can replace
    s by t.inverse(). 
    If the relation st has even length an len(s)=len(t), we choose to replace 
    the longest (lexigographic order) between s and t.inverse() by the shortest, for normalization.
    (see relation_shortening below)
    """
    length = len(relation)
    l1 = ceil((length+1)/2)
    shorts = {relation[:l] : (relation[l:].swapcase()[::-1]) 
                  for l in range(l1, length+1)}
    
    if not length %2:
        s = relation[:l1-1]
        t = relation[l1-1:].swapcase()[::-1]
        if (s>t):
            shorts[s] = t
    return shorts.items()

def simplifiable(word,shortenings):
    """
    Checks if a string can be simplified using a 
    shortening.
    
    Returns a boolean and the simplified string
    """
    for s,t in shortenings.items():
        if s in word:
            return True, word.replace(s,t)
    return False, word
        
def word_simplify(word,shortenings):
    b, w = simplifiable(word,shortenings)
    while b:
        b, w = simplifiable(w,shortenings)
    return w
    
def cyclic_permutations(w):
    l = len(w)
    ww = w*2
    return [ww[k:k+l] for k in range(1,l+1)] 
    
