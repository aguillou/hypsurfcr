#!/usr/bin/env python
# -*- coding: utf-8 -*-



from mpmath import mp, mpc, mpf, matrix, sqrt,\
                    pi, cos, sin, exp, arg, norm, acos, asin,\
                    floor, eigh, sign, diag, fabs
mp.dps = 30
I = mpc(0,1)
import pandas as pd
from ComplexHyperbolicRepresentation import PU21ParametrizedRepresentation,\
                                            goldman_trace
                                            



class DeformTriangleSolution_parametrized_pqr(PU21ParametrizedRepresentation):
    """docstring for DeformTriangleSolution."""

    def __init__(self, alpha, theta, p = 3, q = 3, r = 4, param_store = '',
                    param_key = '', admissible_error = 1e-25):

        self.p = p
        self.q = q
        self.r = r
        self.string_p = f'{p}'
        if p == 0:
            self.string_p = 'inf'
        self.string_q = f'{q}'
        if q == 0:
            self.string_q = 'inf'
        self.string_r = f'{r}'
        if r == 0:
            self.string_r = 'inf'
        name = self.string_p + self.string_q + self.string_r
        
        self.__check_parameters = check_parameters(alpha, theta, p, q, r)
        
        if not self.__check_parameters:
            raise ValueError('Not a suitable triangular invariant')


        self.parameters = (mpf(alpha),mpf(theta))
        parameter_name = "%.3f_%.3f"%(alpha,theta)
        
        GENERATORS = ['a','b','c']
        RELATIONS = set(['abC'])
        if p>0:
            RELATIONS.add('a'*p)
        if q>0:
            RELATIONS.add('b'*q)
        if r>0:
            RELATIONS.add('c'*r)
            
                   
        REPRESENTATION = self.__define_dict()
        
        super().__init__(GENERATORS,
                        RELATIONS,
                        REPRESENTATION,
                        name = name,
                        admissible_error = admissible_error,
                        param_store = param_store,
                        param_key = param_key,
                        param_name = parameter_name)

    def change_parameter(self,new_alpha, new_theta, admissible_error = 1e-25):
        try:
            self.parameters = (mpf(new_alpha),mpf(new_theta))
            new_parameter_name = "%.3f_%.3f"%(new_alpha,new_theta)
            New_Rep = self.define_dict()
            self.change_representation(
                        New_Rep, param_name = new_parameter_name)
        except:
            raise Exception('Not a PU(2,1) representation')
        

        
    def __define_dict(self):
        
        return define_dict(self.parameters[0],
                    self.parameters[1],
                    self.p,
                    self.q,
                    self.r)
        

def r_s(p):
    if p == 0:
        return (mpc(1),mpc(0))
    else:
        return (cos(pi/p), sin(pi/p))
        

        
def borne_theta(p,q,r):
    try:
        ma = max([p,q,r])
        return (pi/ma, 2*pi-pi/ma)
    except:
        return (0, 2*pi)
        
def compute_angles_sin(theta,p,q,r):
    s1 = sin(pi/p)/sin(theta/2)
    s2 = sin(pi/q)/sin(theta/2)
    s3 = sin(pi/r)/sin(theta/2)
    return s1,s2,s3
 
def borne_alpha(s1,s2,s3):
    r_1 = sqrt(1-s1**2)
    r_2 = sqrt(1-s2**2)
    r_3 = sqrt(1-s3**2)
    
    max_cos = (r_1**2+r_2**2+r_3**2-1)/(2*r_1*r_2*r_3)
    
    if max_cos >1:
        return (0,2*pi)
    else:
        a_min = acos(max_cos)
        return (a_min, 2*pi-a_min)
        
def check_parameters(alpha,theta,p,q,r):
    check_param = True
    
    min_theta,max_theta = borne_theta(p,q,r)
    if theta<min_theta or theta>max_theta:
        check_param = False
    
    s1, s2, s3 = compute_angles_sin(theta,p,q,r)

    min_alpha,max_alpha = borne_alpha(s1,s2,s3)
    if alpha<min_alpha or alpha>max_alpha:
        check_param = False

    return check_param

        
def define_dict(alpha, theta, p,q,r):
    
    eta = exp(I*theta)
    A_alpha = exp(I*alpha)
    A_alpha_bar = exp(-I*alpha)
    
    R_eta = matrix([[1,0,0],[0,eta,0],[0,0,1]])
    R3=R_eta
    
    s1 = sin(pi/p)/sin(theta/2)
    r1 = sqrt(1-s1**2)
    M1 = matrix([[r1,s1,0],[-s1,r1,0],[0,0,1]])
    
    R2 = M1*R_eta*(M1**-1)

    s2 = sin(pi/q)/sin(theta/2)
    r2 = sqrt(1-s2**2)
    
    s3 = sin(pi/r)/sin(theta/2)
    r3 = sqrt(1-s3**2)
    
    try: #Risk of division by 0
        nu_0 = r3*A_alpha -r1*r2
        nu = nu_0/fabs(nu_0)
        print(nu)
    except:
        raise ValueError('Definition of nu failed')
    R_nu = matrix([[nu,0,0],[0,nu,0],[0,0,1]])
    print(R_nu)
    
    try: #Risk of division by 0
        
        ch_d3 = fabs(nu_0) / (s1*s2) 
        sh_d3 = sqrt( ch_d3**2 - 1 )
        print(ch_d3**2-sh_d3**2)
    except:
        raise ValueError('Definition of d3 failed')
        
    T3 = matrix([[ch_d3, 0, sh_d3],[0,1,0],[sh_d3,0,ch_d3]])
    M2 = matrix([[r2,s2,0],[-s2,r2,0],[0,0,1]])
    
    M = (R_nu**-1)*T3*M2
    
    R1 = M * R_eta * (M**-1)

    
    

    return { 'a' : R2*(R3**-1),
             'b' : R3*(R1**-1),
             'c' : R1*(R2**-1)}
