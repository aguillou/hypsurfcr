#!/usr/bin/env python
# -*- coding: utf-8 -*-



from mpmath import mp, mpc, matrix, sqrt,\
                    pi, cos, exp, arg, norm,\
                    floor, eigh, sign, diag, fabs, sign
mp.dps = 30
I = mpc(0,1)
import pandas as pd
from ComplexHyperbolicRepresentation import PU21ParametrizedRepresentation,\
                                            goldman_trace
                                            



class DeformTriangleSolution_parametrized(PU21ParametrizedRepresentation):
    """docstring for DeformTriangleSolution."""

    def __init__(self, parameter, n = 4, param_store = '',
                    param_key = '', admissible_error = 1e-25):

        self.n = n
        name = f'33{n}'
        if n == 0:
            name = f'33inf'
        self.parameter = mpc(parameter)
        parameter_name = "%.3f_%.3f"%(self.parameter.real,self.parameter.imag)
        
        GENERATORS = ['a','b','c']
        RELATIONS = set(['abC'])
        p,q,r = 3,3, self.n
        if p>0:
            RELATIONS.add('a'*p)
        if q>0:
            RELATIONS.add('b'*q)
        if r>0:
            RELATIONS.add('c'*r)
            
                   
        REPRESENTATION = self.define_dict()
        
        super().__init__(GENERATORS,
                        RELATIONS,
                        REPRESENTATION,
                        name = name,
                        admissible_error = admissible_error,
                        param_store = param_store,
                        param_key = param_key,
                        param_name = parameter_name)

    def change_parameter(self,new_parameter, admissible_error = 1e-25):
        try:
            self.parameter = mpc(new_parameter)
            new_parameter_name = "%.3f_%.3f"%(self.parameter.real,self.parameter.imag)
            New_Rep = self.define_dict()
            self.change_representation(
                        New_Rep, param_name = new_parameter_name)
        except:
            raise Exception('Not a PU(2,1) representation')
        
        
        
    def define_dict(self):

        if self.n>0:
            z1 = 4*(cos(pi/self.n)**2) - 1 # tr(c)
        else:
            z1 = mpc(3)

        z2 = self.parameter
        z3 = z1
        z4 = z2.conjugate()

        if goldman_trace(z2) < -1e-10:
            raise ValueError('Ab is elliptic. Goldman: ' + str(goldman_trace(z2)))

        Delta = ( z1*z1*z3*z3 - 2*z1*z2*z3*z4 + z2*z2*z4*z4
                - 4*(z1*z1*z1 + z2*z2*z2 + z3*z3*z3 + z4*z4*z4)
                + 18*(z1*z3 + z2*z4) - 27)
        # Delta toujours réel, négatif pour PU(2,1)
        if Delta.real > 1e-8:
            raise ValueError('Not in PU(2,1).')

        delta = sqrt(Delta.real)

        omega = exp(pi*2*I/3)
        omega2 = exp(-pi*2*I/3)

        a = (
            (z1*z3 - z2*z4 + 6*omega*z1 + 6*omega2*z3 + 9 + delta) /
            (omega*z1 + z2 + omega2*z3 + z4 + 3)
            ) / 4
        d = (
            (z1*z3 - z2*z4 + 6*omega2*z1 + 6*omega*z3 + 9 - delta) /
            (omega2*z1 + z2 + omega*z3 + z4 + 3)
            ) / 4

        b = (
            (z1 - z2 + omega2*(z4-z3) + 3*(omega2-1)) * a /
            (z1 + z2 + omega2*(z3 + z4) + 3*omega)
            ) + (omega - 1) * (
            (z1 + omega*(z2+z3) + z4 + 3*omega2) /
            (z1 + z2 + omega2*(z3 + z4) + 3*omega)
            )

        c = (
            (z1 + omega*(z2 - z3) - z4 + 3*(omega-1) ) * d /
            (z1 + omega*(z2 + z3) + z4 + 3*omega2)
            ) + (omega2 - 1) * (
            (z1 + z2 + omega2 * (z3 + z4) + 3*omega) /
            (z1 + omega*z2 + omega*z3 + z4 + 3*omega2)
            )

        m_a = matrix([
            [omega , 0 , 0],
            [omega2 , 1 , 0],
            [b+a, 2*omega*a, omega2]
        ])

        m_b = matrix([
            [omega, 2*omega2*d, c+d],
            [0, 1, omega],
            [0, 0, omega2]
        ])

        # CR basis transformation

        s3 = sqrt(3)
        l2 = ( (s3*(d.imag*c.real - c.imag*d.real) - d*d.conjugate())/3
               + (s3 * (a+b+c+d).imag -3*(a+b-c).real - d.real)/4)
        s3I = s3*I
        u = -d*(s3I - 1) / (s3I + 3)
        v = (c*s3I + d)/6
        w = -(3*l2*(-s3I+1)
              - (c*(s3I+3)
                 + d*(s3I - 1)) *d.conjugate()
              ) / ( 3*s3I+9 )

        H = matrix([
            [-1/2, u, v],
            [u.conjugate(), l2, w],
            [v.conjugate(), w.conjugate(), -1/2]
        ])
        self.H = H
        
        H_tr = (l2-1).real
        H_det = ( (l2/4 + 2*(u.conjugate()*v*w.conjugate()).real) +
                 1/2*(u*u.conjugate() + w*w.conjugate() - 2*v*v.conjugate()*l2)
                 ).real

        if sign(H_tr) == sign(H_det):
            raise ValueError('Matrices not in PU(2,1).')

        self.m,self.M = CR_basis_transform(H)
        
        m_a = self.M * m_a * self.m
        m_b = self.M * m_b * self.m
        m_c = m_a*m_b

        m_p = (m_a**-1)*m_b # 2123
        if goldman_trace(sum(m_p[i,i] for i in range(3))) < -1e-10:
            raise ValueError('Ab is elliptic.')

        m_com = m_a * m_b * (m_a**-1) * (m_b**-1)
        if goldman_trace(sum(m_com[i,i] for i in range(3))) < -1e-10:
            raise ValueError('[a,b] is elliptic.')

        return { 'a' : m_a,
                 'b' : m_b,
                 'c' : m_c}

def CR_basis_transform(H):
    """
    H is a hermitian matrix of signature 2,1
    
    Returns M and M**-1 such that
    M.transpose_conj() * H * M is the diagonal (-1,-1,1)
    """
    E, Q = eigh(H)
    # We normalize a bit to hope and have continuity with H
    Q[:,0] *= sign(Q[0,0].real)
    Q[:,1] *= sign(Q[0,1].real)
    Q[:,2] *= sign(Q[0,2].real)
    
    delta = diag([1/sqrt(fabs(d)) for d in E])

    M = Q * delta
    M_inv = (delta**-1) * Q.transpose_conj()
    return (M,M_inv)
