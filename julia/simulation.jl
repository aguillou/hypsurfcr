include("arithmetic.jl")

using .Arithmetic
using GLMakie
using DelimitedFiles


type_different = false

triangle_group = trgrp_init()
runned_triangle = trgrp_init()

const P_T! = type_different ? scatter! : lines!

last_p = [Complex(0.)]

function simulation!(tr,eps)

    if last_p[1] != tr
        compute_parameter!(triangle_group,tr)

        #if type_different
        #    compute_parameter!(runned_triangle,tr,4)
        #end

        #try
        #    run(`rm output`)
        #catch
        #    nothing
        #end
        #try
        #    run(`rm output-siegel`)
        #catch
        #    nothing
        #end

        queue3,queue2 = execute_adaptive(triangle_group,eps,type_different,runned_triangle)

        delete!(lscene,scattered[1])
        delete!(rscene,scattered[2])

        #data = readdlm("output",' ',Float64)
        #x = data[:,1]
        #y = data[:,2]
        #z = data[:,3]
        #t = data[:,4]

        #dataR = readdlm("output-siegel",' ',Float64)
        #xR = dataR[:,1]
        #yR = dataR[:,2]
        #tR = dataR[:,3]

        scattered[1] = P_T!(lscene,[d[1] for d in queue3],
                                   [d[2] for d in queue3],
                                   [d[3] for d in queue3];
                                   color=[d[4] for d in queue3])
        scattered[2] = P_T!(rscene,[d[1] for d in queue2],
                                   [d[2] for d in queue2];
                                   color=[d[3] for d in queue2])

        #if type_different

        #    scattered[1] = P_T!(lscene,x,y,z;markersize = 4)
        #    scattered[2] = P_T!(rscene,xR,yR;markersize = 1)

        #else

        #    scattered[1] = P_T!(lscene,x,y,z;color=t)
        #    scattered[2] = P_T!(rscene,xR,yR;color=tR)

        #end
    end

    last_p[1] = tr

end

fig = Figure(resolution=(2500,1500))

lsgrid = labelslidergrid!(
    fig,
    ["eps (-log)",
    "x ",
    "iy "],
    [0.5:0.1:2.5, 1:0.01:8, 0:0.01:1.5];
    horizontal=false,
    tellheight = false)

set_close_to!(lsgrid.sliders[1], 1.2)
set_close_to!(lsgrid.sliders[2], 8.)
set_close_to!(lsgrid.sliders[3], 0.)

sliderobservables = [s.value for s in lsgrid.sliders]

eps = lift(sliderobservables[1]) do s1
    exp(-Float64(s1)*log(10))
end

p_tr = lift(sliderobservables[2],sliderobservables[3]) do s1,s2
    Complex(Float64(s1) + Float64(s2) * 1im)
end

p_point = lift(sliderobservables[2],sliderobservables[3]) do x, y
       Point2f(x, y)
end

button = Button(fig,label="Run",width=Relative(0.8))

on(button.clicks) do k
    println(p_tr.val, " ", eps.val)
    simulation!(p_tr.val,eps.val)
end

controls_grid = GridLayout()
controls_grid[1,1] = lsgrid.layout
controls_grid[2,1] = button

fig.layout[1,3] = controls_grid
colsize!(fig.layout,2,Relative(0.3))
colsize!(fig.layout,3,Relative(0.15))


lscene = LScene(fig[1,1], scenekw = (camera = cam3d!, raw = false))
rscene = Axis(fig)
pscene = Axis(fig)

plot_grid = GridLayout()
plot_grid[1,1] = rscene
plot_grid[2,1] = pscene
fig.layout[1,2] = plot_grid


f_delta(x) = sqrt(-x^2 + 4*sqrt(2*x + 9)*x - 12*x)
f_ell(x) = sqrt(x * ( -x + 4* sqrt(2*x + 3) - 12) + 6* sqrt(2*x + 3) - 9)

t = -5:0.1:5
scattered = [
P_T!(lscene,cos.(t),cos.(t),sin.(t);color=t),
P_T!(rscene,cos.(t),sin.(t);color=t)
]

xx = 0:0.01:8
lines!(pscene,xx,f_delta.(xx),color=:red)
xx= 0:0.01:3
lines!(pscene,xx,f_ell.(xx),color=:blue)

scatter!(pscene,p_point,color=:green,markersize=15)
limits!(pscene,0,8,0,1.5)

mousemarker = [8.,0.]
p_point = [8.,0.]
mousemarker = lift(events(fig).mouseposition) do mp
    # `mp` is relative to the window, throw it away.
    r = [mouseposition(pscene.scene)]

    #Real time
    p = r[1]

    if (p[1] >= 0 &&
        p[1] <= 8 &&
        p[2] >= 0)

        p_point[1] = p[1]
        p_point[2] = p[2]

        if p_point[2] > f_delta(p_point[1])
            p_point[2] = f_delta(p_point[1])
        end
        if p_point[1] < 3 && p_point[2] < f_ell(p_point[1])
            p_point[2] = f_ell(p_point[1])
        end

        set_close_to!(lsgrid.sliders[2], p_point[1])
        set_close_to!(lsgrid.sliders[3], p_point[2])

        p_tr = p_point[1] + p_point[2]*1im
        println("\n",p_tr, " ", eps.val)
        simulation!(p_tr,eps.val)
    end

    return r
  end

simulation!(8.,1e-2)
display(fig)



# julia --sysimage sys_sim.so
# create_sysimage(["GLMakie","DelimitedFiles","StaticArrays","LinearAlgebra"], sysimage_path="sys_sim.so", precompile_execution_file="simulation.jl")


# dataR = readdlm("output-siegel",' ',Float64) ; xR = dataR[:,1] ; yR = dataR[:,2] ; tR = dataR[:,3] ; scatter(xR,yR;color=tR,markersize=1)
