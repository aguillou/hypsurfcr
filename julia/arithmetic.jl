module Arithmetic

include("triangle.jl")

export compute_parameter!, trgrp_init
export hausdorff, execute_adaptive, N_FT,execute!
export seq, seq_alt!, apply_seq, apply_seq_alt

using Printf, LinearAlgebra, GenericSchur
using StaticArrays
using Dates

using .TriangleGroup

struct Node
    type::UInt8
    hash1::UInt64
    hash2::UInt64
    bot::UInt16
end

struct SeqTree
    tree::Vector{Node}
    index::Vector{UInt64}
end


function seq!(seqtree,p,q)
    # Renvoie un arbre de calcul pour accéder au point3Rp/q

    # si mot de la forme "abAb" alors tree[i] = [0,hash("abAB"),0]
    # si mot de la forme "abAb..." alors tree[i],.. = [0,hash("abAB"),0],..
    # si mot de la forme "c100110" alors tree[i] = [1,100,110]
    # si mot de la forme "C100110" alors tree[i] = [2,100,110]

    # a  :       p,q = p-q , p
    # A  :       p,q =   q , -p+q
    # b  :       p,q =  -q , p+q
    # B  :       p,q = p+q , -p

    # ab=c :     p,q = p+2q, q
    # BA=C :     p,q = p-2q, q

    # aB=d :     p,q = 2p+q, p+q
    # bA=D :     p,q = p-q, -p+2q

    # le hash d'un mot doit permettre de retrouver le mot inverse
    # hash_w_alt('ab') = hash_w('BA')

    # si un mot est de taille > max
    #  w_1 + w_2
    # alors les invs sont W_2 + W_1
    # donc hash de w_2 puis hash de w_1

    # exemple:
    # 1/2 -> -1/1 par a
    #  -> 0/1 par B
    # res = (Ba)^{-1} = Ab
    # hash Ab  = 1001 = 9

    # on commence avec hash = 1
    # a -> A -> hash << 2
    # B->b -> hash = hash << 1 + 1

    idx = 0
    current_length_word = 0
    number = UInt64(1)

    while p != 0

        if current_length_word == max_length

            seqtree.tree[idx+1] = Node(0,number,0,0)
            idx += 1

            current_length_word = 0
            number = UInt64(1)

        end

        if q < 0

            p *= -1
            q *= -1

        end

        if q == 0

            if p > 0
                #word *= 'a'
                number <<= 1
                current_length_word += 1
            else
                #word *= 'B'
                number <<= 2
                number += 3
                current_length_word += 1
            end

            p = 0

        elseif p > 0

            if p >= 4*q # c = ab

                n = p÷(2*q)
                p,q = p-n*2*q,q

                if current_length_word > 0
                    seqtree.tree[idx+1] = Node(0,number,0,0)
                    idx += 1
                    number = UInt64(1)
                    current_length_word = 0
                end

                hash = hash_c(n)
                seqtree.tree[idx+1] = Node(1,hash[1],hash[2],hash[3])
                idx += 1

            end

            if p <= q
                p,q = p-q,p
                #word *= 'A'
                number <<= 2
                current_length_word += 1
            else
                p,q = q, q-p
                #word *= 'a'
                number <<= 1
                current_length_word += 1
            end

        else # p < 0

            if -p >= 4*q # C = BA

                n = -p÷(2*q)
                p,q = p+n*2*q,q

                if current_length_word > 0
                    seqtree.tree[idx+1] = Node(0,number,0,0)
                    idx += 1
                    number = UInt64(1)
                    current_length_word = 0
                end

                hash = hash_c(n)
                seqtree.tree[idx+1] = Node(2,hash[1],hash[2],hash[3]+1)
                idx += 1

            end

            if -p > q
                p,q = -q,p+q
                #word *= 'B'
                number <<= 2
                number += 3
                current_length_word += 1
            else
                p,q = p+q,-p
                #word *= 'b'
                number <<= 1
                number += 1
                current_length_word += 1
            end

        end

    end

    if current_length_word > 0
        seqtree.tree[idx+1] = Node(0,number,0,0)
        idx+=1
    end

    seqtree.index[1] = idx

end

function apply_seq(seqtree,point3C,triangle_group)

    for node in reverse(seqtree.tree[1:seqtree.index[1]])

        if node.type == 0

            point3C = triangle_group.matrices_ab[node.hash1] * point3C
            point3C /= point3C[3]

        elseif node.type >= 1

            pointeur = node.type == 1 ? triangle_group.matrices_c : triangle_group.matrices_C

            if node.bot > node.type - 1
                point3C = triangle_group.matrices_c_short[node.bot] * point3C
                point3C /= point3C[3]
            end

            n1 = node.hash1
            n2 = node.hash2

            pow = max_length_c

            while n1!= 0

                if n1%2 == 1
                    point3C = pointeur[pow] * point3C
                    point3C /= point3C[3]
                end

                n1 >>= 1
                pow += 1

            end

            pow = max_length_c + 64

            while n2!= 0

                if n2%2 == 1
                    point3C = pointeur[pow] * point3C
                    point3C /= point3C[3]
                end

                n2 >>= 1
                pow += 1

            end

        end

    end

    return point3C
end

#point base projection
# [1+1, 1-1] en arc
#const b = [( 1 - 1im)/N_FT(2),
#           ( 1 + 1im)/N_FT(2)]

function stereographic!(point3R,point3C)
    #quaternion_div!(x_point)

    #a = point3C[1] + point3C[2]
    #b = point3C[1] - point3C[2]

    #x =  a + b*1im
    #x = x_point[1] * conj(b[1]) + x_point[2] * conj(b[2])
    #y = -b - a*1im
    #y = - x_point[1] * b[2] + x_point[2] * b[1]

    #c = -real(x) + 1
    #point3C /= point3C[3]
    c = (-real(point3C[1]) + 1)

    point3R[1] = imag(point3C[1]) / c
    point3R[2] = real(point3C[2]) / c
    point3R[3] = imag(point3C[2]) / c
end

function siegel!(point2R,point3C)
    x =  point3C[2] / (-point3C[1] + 1)

    point2R[1] = real(x)
    point2R[2] = imag(x)
end


norm1_2(x::Vector{N_FT}) = abs(x[1]) + abs(x[2])

f(x) = sin(2*atan(1/x))

function compute!(point3R,point2R,r,s,seqtree,triangle_group)
    seq!(seqtree,r,s)
    applied = apply_seq(seqtree,triangle_group.spe_points[1],triangle_group)
    stereographic!(point3R,applied)
    siegel!(point2R,applied)
end

function compute(r,s,seqtree,triangle_group)
    seq!(seqtree,r,s)
    return apply_seq(seqtree,triangle_group.spe_points[1],triangle_group)
end

function export!(file,fileR,point3,point2,rational)
    r = f(rational)
    @printf(file, "%.8f %.8f %.8f %.8f\n",
            point3[1],
            point3[2],
            point3[3],
            r)

    @printf(fileR, "%.8f %.8f %.8f\n",
            point2[1],
            point2[2],
            r)
end

function exportfile!(queue3,queue2)

    file = open("output","a+")
    fileR = open("output-siegel","a+")

    t = Dates.now()
    for p in queue3
        @printf(file, "%.8f %.8f %.8f %.8f\n",
                       p[1],p[2],p[3],p[4])
    end
    for p in queue2
        @printf(fileR, "%.8f %.8f %.8f\n",
                       p[1],p[2],p[3])
    end

    close(file)
    close(fileR)

end

function appexp!(queue3,queue2,point3,point2,rational)
    r = f(rational)
    push!(queue3,(point3[1],point3[2],point3[3],r))
    push!(queue2,(point2[1],point2[2],r))
end

function execute_adaptive(triangle_group, epsilon_sup,
                          type_different = false,
                          runned_triangle = triangle_group)

    time = Dates.now()

    Q = MAX_PRECISION ? BigInt(10_000) : Int64(100)

    min_Q = 100
    max_Q = MAX_PRECISION ? BigInt(10)^150 : Int64(10)^18
    Q_factor = 10

    seqtree = SeqTree(Vector{Node}(undef, MAX_PRECISION ? 500 : 30),[0])

    epsilon_inf = epsilon_sup / 10

    point3R = zeros(N_FT,3)
    point3R_bis = point3R[:]

    point2R = zeros(N_FT,2)
    point2R_bis = point2R[:]

    point2R_0 = point2R[:]
    point3R_1 = point3R[:]

    stereographic!(point3R,triangle_group.v_0)
    siegel!(point2R,triangle_group.v_0)

    queue3 = [(point3R[1],point3R[2],point3R[3],f(0.))]
    queue2 = [(point2R[1],point2R[2],f(0.))]

    #if type_different

    #    stereographic!(point3R,runned_triangle.v_0)
    #    siegel!(point2R,runned_triangle.v_0)

    #end

    incr = N_IT(1)
    order = true
    negative = false
    i = N_IT(0)

    siegel!(point2R_0,triangle_group.v_0)

    for k in 1:4

        if k == 1 # 0 -> 1
            i = N_IT(1)
            incr = N_IT(1)
            negative = false
            order = true

        elseif k == 2 # 1 -> infty
            i = Q
            incr = N_IT(-1)
            negative = false
            order = false

        elseif k == 3 # infty -> -1
            i = N_IT(-1)
            incr = N_IT(-1)
            negative = true
            order = false

        elseif k == 4 # -1 -> 0
            i = -Q
            incr = N_IT(1)
            negative = true
            order = true

        end

        while abs(i) <= Q && (negative ? i <= 0 : i >= 0)

            r = order ? i : Q
            s = order ? Q : i

            new_point = compute(r,s,seqtree,triangle_group)
            siegel!(point2R,new_point)

            norm_delta = norm1_2(point2R-point2R_0)

            if norm_delta > epsilon_sup

                while norm_delta > epsilon_sup && Q < max_Q

                    i = (i-incr)*Q_factor + incr
                    Q *= Q_factor

                    r = order ? i : Q
                    s = order ? Q : i

                    new_point = compute(r,s,seqtree,triangle_group)
                    siegel!(point2R,new_point)

                    norm_delta = norm1_2(point2R-point2R_0)

                    #if MAX_PRECISION && Q >= max_Q
                    #    println("alert max")
                    #end
                end

            elseif norm_delta < epsilon_inf

                i_try = i
                Q_try = Q

                while norm_delta < epsilon_inf && Q > min_Q

                    i_try = (i_try-incr)÷Q_factor + incr
                    Q_try = Q_try÷Q_factor

                    r = order ? i_try : Q_try
                    s = order ? Q_try : i_try

                    new_point_bis = compute(r,s,seqtree,triangle_group)
                    siegel!(point2R_bis,new_point_bis)

                    norm_delta = norm1_2(point2R_bis-point2R_0)

                    if norm_delta < epsilon_sup

                        new_point = new_point_bis

                        point2R[1] = point2R_bis[1]
                        point2R[2] = point2R_bis[2]

                        i = i_try
                        Q = Q_try

                    end

                    #if MAX_PRECISION && Q < min_Q
                    #    println("alert min")
                    #end

                end

            end

            point2R_0[1] = point2R[1]
            point2R_0[2] = point2R[2]

            stereographic!(point3R,new_point)

            i += incr

            rational = order ? Float64(i/Q) : Float64(Q/i)

            #if type_different

            #    r = order ? i : Q
            #    s = order ? Q : i

            #    compute!(point3R,point2R,r,s,seqtree,runned_triangle)

            #end

            appexp!(queue3,queue2,point3R,point2R,rational)
        end

    end

    time = Float64(Dates.value(Dates.now()-time)/1000)
    println(length(queue2), " points in ", time, " seconds.")

    return (queue3,queue2)
    #exportfile!(queue3,queue2)
end

end  # module Arithmetic





# include("arithmetic.jl") ; using .Arithmetic ; tr = trgrp_init() ; compute_parameter!(tr,3.) ; execute_adaptive(tr,0.001)
