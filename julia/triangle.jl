module TriangleGroup

export N_FT, N_IT, MAX_PRECISION
export compute_parameter!, trgrp_init
export max_length, max_length_c
export hash_w,hash_c

using LinearAlgebra, GenericSchur
using StaticArrays

const max_C = 120 # 2 UInt64
const max_length = 6
const max_length_c = 8

const MAX_PRECISION = false

const N_FT = MAX_PRECISION ? BigFloat : Float64
const N_IT = MAX_PRECISION ? BigInt : Int64

hash_c(n::BigInt) =  ((n >> 0x08) & 0xffffffffffffffff,
                      (n >> 0x48),
                      (n & 0xff)  << 0x01 )
hash_c(n::Int64) =  ((n >> 0x08), #& 0xffffffffffffffff,
                     (n >> 0x48),
                     (n & 0xff)  << 0x01 )

struct Triangle_Group
    v_0 :: Vector{Complex{N_FT}}
    v_infty :: Vector{Complex{N_FT}}

    spe_points ::  Array{StaticArrays.SVector{3,Complex{N_FT}},1}

    matrices_ab :: Array{StaticArrays.SMatrix{3, 3, Complex{N_FT}, 9},1}
    matrices_c :: Array{StaticArrays.SMatrix{3, 3, Complex{N_FT}, 9},1}
    matrices_C :: Array{StaticArrays.SMatrix{3, 3, Complex{N_FT}, 9},1}
    matrices_c_short :: Array{StaticArrays.SMatrix{3, 3, Complex{N_FT}, 9},1}
end

function trgrp_init()
    return Triangle_Group(
    zeros(Complex{N_FT},3),
    zeros(Complex{N_FT},3),
    Array{StaticArrays.SVector{3,Complex{N_FT}},1}(undef,2),
    Array{StaticArrays.SMatrix{3, 3, Complex{N_FT}, 9},1}(undef,2^(2*max_length)*2),
    Array{StaticArrays.SMatrix{3, 3, Complex{N_FT}, 9},1}(undef, max_C+1),
    Array{StaticArrays.SMatrix{3, 3, Complex{N_FT}, 9},1}(undef, max_C+1),
    Array{StaticArrays.SMatrix{3, 3, Complex{N_FT}, 9},1}(undef, 2^(max_length_c+1))
    )
end

function compute_parameter!(triangle_group,parameter,tr_type=0)

    z1 = Complex(N_FT(3))

    if tr_type == 4
        z1 = Complex(N_FT(1))
    elseif tr_type > 0
        z1 = Complex(4*cos(N_FT(pi)/tr_type)^2 - 1)
    end

    z2 = parameter
    z3 = z1
    z4 = conj(z2)

    Delta = (z1*z1*z3*z3 - 2*z1*z2*z3*z4 + z2*z2*z4*z4
            - 4*(z1*z1*z1 + z2*z2*z2 + z3*z3*z3 + z4*z4*z4)
            + 18*(z1*z3 + z2*z4) - 27)

    if real(Delta) > 0
        print("Not in SU(2,1): ",Delta)
        Delta = 0
    end

    delta = sqrt(-real(Delta))*1im

    omega = exp((N_FT(pi)*2/3)*1im)
    omega2 = exp(-(N_FT(pi)*2/3)*1im)

    a = (
        (z1*z3 - z2*z4 + 6omega*z1 + 6omega2*z3 + 9 + delta) /
        (omega*z1 + z2 + omega2*z3 + z4 + 3)
        ) / 4

    d = (
        (z1*z3 - z2*z4 + 6omega2*z1 + 6omega*z3 + 9 - delta) /
        (omega2*z1 + z2 + omega*z3 + z4 + 3)
        ) / 4

    b = (
        (z1 - z2 + omega2*(z4-z3) + 3(omega2-1)) * a /
        (z1 + z2 + omega2*(z3 + z4) + 3omega)
        ) + (omega - 1) * (
        (z1 + omega*(z2+z3) + z4 + 3omega2) /
        (z1 + z2 + omega2*(z3 + z4) + 3omega)
        )

    c = (
        (z1 + omega*(z2 - z3) - z4 + 3(omega-1) ) * d /
        (z1 + omega*(z2 + z3) + z4 + 3omega2)
        ) + (omega2 - 1) * (
        (z1 + z2 + omega2 * (z3 + z4) + 3omega) /
        (z1 + omega*z2 + omega*z3 + z4 + 3omega2)
        )

    m_a = @SMatrix [omega  0          0
           omega2 1          0
           a+b    2*omega*a  omega2]

    m_b = @SMatrix [omega  2*omega2*d  c+d
           0      1           omega
           0      0           omega2]

    # CR basis transformation

    sqrt3 = sqrt(N_FT(3))
    i_sqrt3 = sqrt3*1im

    l2 = (
         ( sqrt3 * (imag(d)*real(c) - imag(c)*real(d)) - abs2(d) ) / 3
         + ( sqrt3 * imag(a+b+c+d) - real(a+b-c)*3 - real(d) ) / 4
         )

    u = -d* ( i_sqrt3 - 1 ) / (i_sqrt3 + 3)
    v = ( c * i_sqrt3 + d ) / 6
    w = - ( 3 * l2 *(-i_sqrt3 + 1)
            - (c*(i_sqrt3 + 3) + d*(i_sqrt3 - 1)) * conj(d)
          ) / (3 * i_sqrt3 + 9)


    H = @SMatrix [  -1/2   u        v
                    conj(u) l2       w
                    conj(v) conj(w)  -1/2]
    H = Hermitian(H)

    m,M = CR_basis_transform(H)

    m_a = M*m_a*m
    m_b = M*m_b*m


    m_A = m_a*m_a
    m_B = m_b*m_b

    m_ab = m_a*m_b
    m_BA = m_B*m_A


    v_infty_eig = eigvecs(m_ab)[1:3]

    if tr_type != 0
        v_infty_eig = eigvecs(m_a*m_b*m_A*m_B)[1:3]
    end

    v_infty_eig = v_infty_eig / v_infty_eig[3]
    triangle_group.v_infty[1] = v_infty_eig[1]
    triangle_group.v_infty[2] = v_infty_eig[2]
    triangle_group.v_infty[3] = v_infty_eig[3]

    triangle_group.spe_points[2] = SVector(v_infty_eig[1],v_infty_eig[2],v_infty_eig[3])

    v_0_e = m_b * triangle_group.v_infty
    v_0_e =  v_0_e / v_0_e[3]
    triangle_group.v_0[1] = v_0_e[1]
    triangle_group.v_0[2] = v_0_e[2]
    triangle_group.v_0[3] = v_0_e[3]

    triangle_group.spe_points[1] = SVector(v_0_e[1],v_0_e[2],v_0_e[3])

    triangle_group.matrices_c[1] = m_ab
    triangle_group.matrices_C[1] = m_BA

    for i in 1:max_C

        triangle_group.matrices_c[i+1] =  triangle_group.matrices_c[i]^2
        triangle_group.matrices_C[i+1] =  triangle_group.matrices_C[i]^2

    end

    characters = ['a','b','A','B']
    base_matrices = [m_a,m_b,m_A,m_B]

    for i in 1:4
        triangle_group.matrices_ab[hash_w(string(characters[i]))] = base_matrices[i]
    end

    words = ["a","b","A","B"]
    matrices = [m_a,m_b,m_A,m_B]

    for i in 2:max_length

        n = length(words)
        for j in 1:n

            word = words[j]
            matrix = matrices[j]

            for k in 1:length(characters)

                char = characters[k]
                base_matrix = base_matrices[k]

                if char != (isuppercase(word[end]) ? lowercase(word[end]) : uppercase(word[end]))

                    new_word = word*char
                    new_mat = matrix * base_matrix

                    push!(words, new_word)
                    push!(matrices, new_mat)

                    triangle_group.matrices_ab[hash_w(new_word)] = new_mat

                end

            end

        end
    end

    for i in 1:(2^max_length_c)-1

        str_i = reverse(string(i,base = 2))

        mat_c = @SMatrix [one(Complex{N_FT}) zero(Complex{N_FT}) zero(Complex{N_FT})
                          zero(Complex{N_FT}) one(Complex{N_FT}) zero(Complex{N_FT})
                          zero(Complex{N_FT}) zero(Complex{N_FT}) one(Complex{N_FT})]

        mat_C = @SMatrix [one(Complex{N_FT}) zero(Complex{N_FT}) zero(Complex{N_FT})
                          zero(Complex{N_FT}) one(Complex{N_FT}) zero(Complex{N_FT})
                          zero(Complex{N_FT}) zero(Complex{N_FT}) one(Complex{N_FT})]

        for j in 1:length(str_i)

            if str_i[j] == '1'
                mat_c *= triangle_group.matrices_c[j]
                mat_C *= triangle_group.matrices_C[j]
            end
        end

        n  = i << 1

        triangle_group.matrices_c_short[n] = mat_c
        triangle_group.matrices_c_short[n+1] = mat_C
    end
end

function CR_basis_transform(H)

    Q = Matrix(eigvecs(H))

    Q[:,1] *= exp(-imag(log(Q[1,1]))*1im)
    Q[:,2] *= exp(-imag(log(Q[1,2]))*1im)
    Q[:,3] *= exp(-imag(log(Q[1,3]))*1im)

    Q_inv = adjoint(Q)
    #N = eigvals(H)
    N = adjoint(Q)*H*Q

    N = Diagonal(N)
    N = sqrt.(abs.(real.(N)))

    delta = N
    delta_inv = inv(N)

    M = delta * Q_inv
    m = Q * delta_inv

    return (m,M)

end

function hash_w(word)

    number = UInt64(0)

    j_2 = UInt64(1)

    for  letter in reverse(word)

        if letter == 'a' # => 0
            j_2 <<= 1

        elseif letter == 'b' # => 1
            number += j_2
            j_2 <<= 1

        elseif letter == 'A' # => 00
            j_2 <<= 2

        else # 'B' # => 11
            number += j_2 + (j_2<<1)
            j_2 <<= 2
        end

    end

    return number + j_2
end



end # END MODULE
