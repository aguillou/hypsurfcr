# TODO



## AbstractPresentedGroup

Document the code!

## HyperbolicPresentedGroup

Optimize the computation of the parametrized
 boundary (especially the for loop computing 
 attractive fixed points)

## TODO: FundamentalDomains and triples.

Not yet implemented at all.

## ComplexHyperbolicRepresentation

Optimize, if possible, the computation of limit set 
(especially the for loop computing attractive fixed 
points)

In export_csv: add a filter so that identified 
points are not repeated. Keep an index along the way. cf numpy.around and pandas.groupby https://www.statology.org/pandas-groupby-aggregate-multiple-columns/. Beware: we just want to collapse successive indices!

## ParametrizedTriangleGroup

Write a method to recompute a parametrization (i.e. be able to construct the associated HyperbolicPresentedGroup)

## Vizualisation

Streamline the visualization process:

- add the possibility to plot the snapshots without exporting/importing csv

- add the needed code for building the website (paraview+ javascript+csv...)
